/** Prepare variables */
var ssid = null;
var password = null;
var qrLink = null;
var qr = document.getElementById("qr");
var qrMobile = document.getElementById("qr-mobile");
var input = document.getElementById("section-input");
var output = document.getElementById("section-output");

/** Listen for clickin on generate button */
document.getElementById("generate").addEventListener("click", function(){

    /** Show QR Code */
    document.querySelectorAll("#qr")[0].style.opacity = "0";

    /** Clear previous QR code */
    qr.innerHTML = "";
    qrMobile.innerHTML = "";

    /** Get username & password */
    ssid = document.getElementById("ssid").value;
    password = document.getElementById("password").value;

    /** If username & password has been inserted */
    if(ssid && password) {

        /** Update widths */
        input.classList.add("opened");
        output.classList.add("opened");

        if (typeof window.orientation == 'undefined') {

            /** Generate QR Code */
            new QRCode(qr, {
                text: "WIFI:S:" + ssid + ";T:WPA;P:" + password + ";;",
                width: 2048,
                height: 2048,
                colorDark : "#181A1C",
                colorLight : "#ffffff"
            });

        } else {

            /** Generate QR Code */
            new QRCode(qrMobile, {
                text: "WIFI:S:" + ssid + ";T:WPA;P:" + password + ";;",
                width: 2048,
                height: 2048,
                colorDark : "#181A1C",
                colorLight : "#ffffff"
            });

        }

        /** Wait few miliseconds (fixes flickering) */
        setTimeout(() => {
                                
            /** Show QR Code */
            if (typeof window.orientation == 'undefined') {
                document.querySelectorAll("#qr")[0].style.opacity = "1";
                document.querySelectorAll("#qr img")[0].style.opacity = "1";
            } else {
                document.querySelectorAll("#qr-mobile")[0].style.opacity = "1";
                document.querySelectorAll("#qr-mobile img")[0].style.opacity = "1";
            }
                       
            /** Set link */
            if (typeof window.orientation == 'undefined') {
                qrLink = document.querySelectorAll("#qr img")[0].getAttribute("src");
                document.getElementById("qr-link").setAttribute("href", qrLink);
                document.getElementById("qr-link").setAttribute("download", ssid + ".png");
            }

        }, 1000);

    }

});